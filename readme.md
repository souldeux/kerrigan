### Master

[![pipeline status](https://gitlab.com/souldeux/kerrigan/badges/master/pipeline.svg)](https://gitlab.com/souldeux/kerrigan/commits/master)

### Edge

[![pipeline status](https://gitlab.com/souldeux/kerrigan/badges/edge/pipeline.svg)](https://gitlab.com/souldeux/kerrigan/commits/edge)

---
Meant to run with the `krang` backend.

`docker network create -d bridge overmind` to create the bridge network

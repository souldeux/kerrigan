from django.http import HttpResponse
from django.views import View
from api.tasks import classify_image
import tempfile, json, base64

class HealthCheck(View):
    def get(self, request):
        return HttpResponse('ok')

class IndexView(View):
    def get(self, request):
        return HttpResponse('index')

from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from .views import *

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^admin/', admin.site.urls),
    url(r'^api/v1/health/', HealthCheck.as_view(), name='health'),
    url(r'^api/v1/', include('api.urls', namespace='api')),
]

#Development staticfile deployment strategy
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

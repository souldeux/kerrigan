FROM alpine:edge

RUN apk --no-cache --update upgrade
RUN apk --no-cache --update add \
    python3 \
    py3-psycopg2 \
    postgresql-client \
    curl

RUN pip3 install --upgrade pip setuptools \
    && rm -rf /root/.cache/pip

ADD requirements.txt /opt/kerrigan/requirements.txt

RUN pip3 install -r /opt/kerrigan/requirements.txt \
  && rm -rf /root/.cache/pip

RUN adduser -D web
USER root

ADD . /opt/kerrigan
WORKDIR /opt
RUN /usr/bin/find . -type d -exec chown web {} \;
RUN /usr/bin/find /opt/kerrigan/bin -type f -exec chmod +x {} \;
WORKDIR /opt/kerrigan
RUN python3 -m compileall -f -q .
USER web

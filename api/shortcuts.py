from django.conf.urls import url, include
from rest_framework import routers

def endpoint_set(views_module):
    router = routers.SimpleRouter()
    router.register(r'users', views_module.UserViewSet, base_name='user')
    router.register(r'pictures', views_module.PictureViewSet, base_name='picture')

    urlpatterns = [
        url(r'^', include(router.urls))
    ]

    return urlpatterns

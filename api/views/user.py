from django.contrib.auth.models import User
from rest_framework import viewsets, mixins
from api.serializers import UserSerializer

class UserViewSet(mixins.RetrieveModelMixin,
        mixins.ListModelMixin,
        mixins.CreateModelMixin,
        mixins.DestroyModelMixin,
        viewsets.GenericViewSet):
    model = User
    queryset = User.objects
    serializer_class = UserSerializer

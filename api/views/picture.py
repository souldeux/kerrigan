from api.models import Picture
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from api.serializers import PictureCreateSerializer, PictureDetailSerializer

class PictureViewSet(mixins.RetrieveModelMixin,
        mixins.ListModelMixin,
        mixins.DestroyModelMixin,
        viewsets.GenericViewSet):
    model = Picture
    queryset = Picture.objects

    def get_serializer_class(self):
        if self.action == 'create':
            return PictureCreateSerializer
        return PictureDetailSerializer

    def create(self, request):
        serializer = self.get_serializer_class()(data = request.data)
        serializer.is_valid(raise_exception = True)
        pic = self.model.objects.create(
            resource = serializer.validated_data['resource']
            )
        #This way we can specify different input & response serializers
        return Response(
            PictureDetailSerializer(pic).data,
            status = 201
            )

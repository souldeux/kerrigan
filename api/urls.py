from . import views
from .shortcuts import endpoint_set

urlpatterns = endpoint_set(views)

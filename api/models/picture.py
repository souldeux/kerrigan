from django.contrib.postgres.fields import JSONField
from .base import *
from uuid import uuid4
from os.path import splitext
from api.constants import *
import tempfile

def unique_filename(instance, filename):
    '''Generate a unique, file-extension-aware filename'''
    filename, ext = splitext(filename)
    if ext == '':
        ext = '.bin'
    return '{}{}'.format(uuid4(), ext)

class Picture(BaseModel):
    resource = models.FileField(upload_to = unique_filename)
    classification_data = JSONField(
        default = dict()
        )

    classification_status = models.CharField(
        default = CLASSIFICATION_STATUS_PENDING,
        blank = False,
        max_length=50,
        help_text = """
        Indicates whether the picture has been successfully classified.
        Will be one of CLASSIFICATION_STATUS_ERROR, CLASSIFICATION_STATUS_PENDING,
        or CLASSIFICATION_STATUS_COMPLETE
        """
        )

    def __str__(self):
        return 'Picture #{}'.format(self.id)

    def download_to_tempfile(self):
        fh = tempfile.NamedTemporaryFile(delete = False)
        fh.write(self.resource.read())
        fh.close()
        filename = fh.name
        fh = None
        return filename

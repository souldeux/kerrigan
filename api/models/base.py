from django.db import models

class BaseModel(models.Model):
    created = models.DateTimeField('created', db_index=True, auto_now_add=True)
    modified = models.DateTimeField('modified', db_index=True, auto_now=True)

    class Meta:
        abstract = True

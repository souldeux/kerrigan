from .picture import Picture
from api.tasks import classify_image
from django.dispatch import receiver
from django.db.models.signals import post_save
from api.constants import *

@receiver(post_save, sender = Picture, dispatch_uid="process_image")
def process_image(sender, instance, **kwargs):
    if instance.classification_status == CLASSIFICATION_STATUS_PENDING:
        classify_image.delay(instance.id)

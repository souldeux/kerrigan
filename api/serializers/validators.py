from rest_framework import serializers

class MaxUploadSize(object):
    def __init__(self, max):
        self.max = max

    def __call__(self, data):
        if data['resource'].size > self.max:
            message = 'This file is larger than the maximum allowed upload size.'
            raise serializers.ValidationError(message)

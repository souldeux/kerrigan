from rest_framework import serializers
from api.models import Picture
from .validators import MaxUploadSize

class PictureCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Picture
        fields = ('resource',)
        validators = [
            MaxUploadSize(26214400)
        ]

class PictureDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Picture
        fields = (
            'id', 'created', 'classification_status', 'classification_data'
        )
        read_only_fields = (
            'id', 'created', 'classification_status', 'classification_data'
        )

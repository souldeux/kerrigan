# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
import requests, json
from django.conf import settings
from api.constants import *
import tempfile
from api.models import Picture

@shared_task
def classify_image(pic_id):
    '''
    Sends an image to Krang for classification. Sets
    `classification_status` as appropriate. Note that
    this expects a Picture instance, not an id.
    '''
    try:
        pic = Picture.objects.get(id = pic_id)
        with open(pic.download_to_tempfile(), 'rb') as f:
            response = requests.post(
                'http://{}:{}/classify'.format(settings.KRANG_HOST, settings.KRANG_PORT),
                files = {'file': f}
            )
        pic.classification_data = json.loads(response.text)
        pic.classification_status = CLASSIFICATION_STATUS_COMPLETE
    except:
        pic.classification_status = CLASSIFICATION_STATUS_ERROR

    pic.save()

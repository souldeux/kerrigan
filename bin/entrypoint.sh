#!/bin/sh

BIND_PT="0.0.0.0:5500"
APP="kerrigan.wsgi:application"
TIMEOUT="60"

python3 manage.py collectstatic --noinput
if python3 manage.py migrate; then
  if [ -n "$DJANGO_DEBUG" -a "$DJANGO_DEBUG" = '1' ]; then
      gunicorn -b $BIND_PT $APP --reload --timeout=$TIMEOUT --workers=4 --keep-alive 900 --log-level=debug
  else
      WORKER_COUNT=$(( 2 * `cat /proc/cpuinfo | grep 'core id' | wc -l` + 1 ))
      gunicorn -b $BIND_PT $APP --timeout=$TIMEOUT --workers=$WORKER_COUNT --keep-alive 900 --log-level=info
  fi
fi
